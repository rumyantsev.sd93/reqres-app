plugins {
    java
    id("org.springframework.boot") version "3.3.0"
    id("io.spring.dependency-management") version "1.1.5"
//    id("com.vaadin") version "24.3.13"
}

group = "ru.rumsd"
version = "0.0.1-SNAPSHOT"

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(17)
    }
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenCentral()
}

extra["vaadinVersion"] = "24.3.13"

dependencies {
//    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    implementation("org.springframework.cloud:spring-cloud-starter-openfeign:4.1.2")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
//    implementation("com.vaadin:vaadin-spring-boot-starter")
//    implementation("org.liquibase:liquibase-core")
    compileOnly("org.projectlombok:lombok")

//    runtimeOnly("org.postgresql:postgresql")
    annotationProcessor("org.projectlombok:lombok")

    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

//dependencyManagement {
//    imports {
//        mavenBom("com.vaadin:vaadin-bom:${property("vaadinVersion")}")
//    }
//}

tasks.withType<Test> {
    useJUnitPlatform()
}
