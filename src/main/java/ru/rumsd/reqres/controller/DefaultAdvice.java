package ru.rumsd.reqres.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import ru.rumsd.reqres.model.ApiFault;

@ControllerAdvice
public class DefaultAdvice {

    @ExceptionHandler
    public ResponseEntity<ApiFault> defaultHandler(Exception e, WebRequest request) {
        var fault = new ApiFault();
        fault.setMessage("Something going wrong");
        return ResponseEntity.badRequest().body(fault);
    }
}
