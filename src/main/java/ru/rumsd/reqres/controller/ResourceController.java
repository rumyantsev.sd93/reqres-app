package ru.rumsd.reqres.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.rumsd.reqres.model.Resource;
import ru.rumsd.reqres.model.ResponseItem;
import ru.rumsd.reqres.model.ResponsePage;
import ru.rumsd.reqres.service.ResourceService;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
@RequiredArgsConstructor
public class ResourceController {
    private final ResourceService resourceService;

    @GetMapping(path = "/resources", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePage<Resource>> resources() {
        return ResponseEntity.ok(resourceService.resources());
    }

    @GetMapping(path = "/resource/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseItem<Resource>> resource(@PathVariable Long id) {
        return ResponseEntity.ok(resourceService.resource(id));
    }
}
