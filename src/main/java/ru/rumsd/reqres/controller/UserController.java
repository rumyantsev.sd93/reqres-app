package ru.rumsd.reqres.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.rumsd.reqres.model.ResponseItem;
import ru.rumsd.reqres.model.ResponsePage;
import ru.rumsd.reqres.model.User;
import ru.rumsd.reqres.service.UserService;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping(path = "/users", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePage<User>> users() {
        return ResponseEntity.ok(userService.users());
    }

    @GetMapping(path = "/user/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseItem<User>> user(@PathVariable Long id) {
        return ResponseEntity.ok(userService.user(id));
    }
}
