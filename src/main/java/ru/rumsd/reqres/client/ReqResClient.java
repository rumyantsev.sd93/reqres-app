package ru.rumsd.reqres.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ru.rumsd.reqres.model.Resource;
import ru.rumsd.reqres.model.ResponseItem;
import ru.rumsd.reqres.model.ResponsePage;
import ru.rumsd.reqres.model.User;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(name = "${reqres.name}", url = "${reqres.api}")
public interface ReqResClient {
    @GetMapping(value = "/users", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    ResponsePage<User> users(@RequestParam Integer page, @RequestParam Integer perPage);

    @GetMapping(value = "/user/{id}", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    ResponseItem<User> user(@PathVariable Long id);

    @GetMapping(value = "/unknown", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    ResponsePage<Resource> resources(@RequestParam Integer page, @RequestParam Integer perPage);

    @GetMapping(value = "/unknown/{id}", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    ResponseItem<Resource> resource(@PathVariable Long id);

}
