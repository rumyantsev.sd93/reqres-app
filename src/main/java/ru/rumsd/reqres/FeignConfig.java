package ru.rumsd.reqres;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Logger;
import feign.Request;
import feign.Response;
import feign.Util;
import lombok.RequiredArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

@Configuration
@RequiredArgsConstructor
public class FeignConfig {

    private final ObjectMapper objectMapper;

    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }


    @Bean
    public Logger customFeignLogger() {
        return new CustomFeignLogger();
    }

    public class CustomFeignLogger extends Logger {
        private final org.slf4j.Logger logger = LoggerFactory.getLogger(CustomFeignLogger.class);

        @Override
        protected void logRequest(String configKey, Level logLevel, Request request) {
            var headers = formatHeaders(request.headers());
            var body = request.body() != null ? new String(request.body()) : "";
            logger.info("Request: {} {}\nHeaders:\n{}\nBody: {}", request.method(), request.url(), headers, body);
        }

        @Override
        protected Response logAndRebufferResponse(String configKey, Level logLevel, Response response, long elapsedTime) throws IOException {
            var headers = formatHeaders(response.headers());
            var body = response.body() != null ? Util.toString(response.body().asReader(Util.UTF_8)) : "";
            body = prettyPrintJson(body, response.headers());
            logger.info("Response: {}\nHeaders:\n{}\nBody: {}", response.status(), headers, body);
            return response.toBuilder().body(body.getBytes(Util.UTF_8)).build();
        }

        @Override
        protected void log(String configKey, String format, Object... args) {
            logger.info(String.format(methodTag(configKey) + format, args));
        }

        private String formatHeaders(Map<String, Collection<String>> headers) {
            return headers.entrySet().stream()
                    .map(entry -> entry.getKey() + "=" + entry.getValue())
                    .collect(Collectors.joining("\n"));
        }

        private String prettyPrintJson(String body, Map<String, Collection<String>> headers) {
            var isJson = headers.getOrDefault("Content-Type", Collections.emptyList()).stream()
                    .anyMatch(value -> value.contains("application/json"));
            if (body != null && !body.isEmpty() && isJson) {
                try {
                    var json = objectMapper.readValue(body, Object.class);
                    return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
                } catch (Exception e) {
                    return "{\"message\": \"serrialization error\"}";
                }
            }
            return body;
        }

    }
}
