package ru.rumsd.reqres;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ReqresApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReqresApplication.class, args);
    }

}
