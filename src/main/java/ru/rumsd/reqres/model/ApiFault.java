package ru.rumsd.reqres.model;

import lombok.Data;

@Data
public class ApiFault {
     private String message;
}
