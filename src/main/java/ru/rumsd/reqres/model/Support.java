package ru.rumsd.reqres.model;

import lombok.Data;

@Data
public class Support {
    private String url;
    private String text;
}
