package ru.rumsd.reqres.model;

import lombok.Data;

@Data
public class ResponseItem<T> {
    private T data;
    private Support support;
}
