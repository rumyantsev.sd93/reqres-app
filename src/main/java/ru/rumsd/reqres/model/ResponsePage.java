package ru.rumsd.reqres.model;

import lombok.Data;

import java.util.List;

@Data
public class ResponsePage<T> {
    private Long page;
    private Long perPage;
    private Long total;
    private Long totalPages;
    private List<T> data;
    private Support support;
}
