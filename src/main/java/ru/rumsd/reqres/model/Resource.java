package ru.rumsd.reqres.model;

public class Resource {
    private Long id;
    private String name;
    private Integer year;
    private String color;
    private String pantoneValue;
}
