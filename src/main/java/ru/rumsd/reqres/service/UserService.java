package ru.rumsd.reqres.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rumsd.reqres.client.ReqResClient;
import ru.rumsd.reqres.model.ResponseItem;
import ru.rumsd.reqres.model.ResponsePage;
import ru.rumsd.reqres.model.User;

@Service
@RequiredArgsConstructor
public class UserService {
    private final ReqResClient client;

    public ResponsePage<User> users() {
        return client.users(1, 10);
    }

    public ResponseItem<User> user(Long id) {
        return client.user(id);
    }

}
