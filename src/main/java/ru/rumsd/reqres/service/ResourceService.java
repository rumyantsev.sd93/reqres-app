package ru.rumsd.reqres.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.rumsd.reqres.client.ReqResClient;
import ru.rumsd.reqres.model.Resource;
import ru.rumsd.reqres.model.ResponseItem;
import ru.rumsd.reqres.model.ResponsePage;

@Service
@RequiredArgsConstructor
public class ResourceService {
    private final ReqResClient client;

    public ResponseItem<Resource> resource(Long id) {
        return client.resource(id);
    }

    public ResponsePage<Resource> resources() {
        return client.resources(1, 10);
    }
}
